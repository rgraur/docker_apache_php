# Apache & PHP Docker image #

### Base image ###
* official [Debian 7.7 image](https://github.com/tianon/docker-brew-debian/tree/94324b3f6941631db52b94cf7f4097fc239e8e68/wheezy)

### Installed packages ###
* apache2
* libapache2-mod-php5
* php 5.5 from [DotDeb repo](http://packages.dotdeb.org/dists/wheezy-php55)
* php5-mysql, php5-mcrypt, php5-curl
* mysql-client

### Specifications ###
* forwarded port: 80
* apache root folder: `/var/www/`

### Run ###
* build image: `docker build -t web .`
* create container: `docker run -p 80:80 -v /host/app/path:/var/www -d web`