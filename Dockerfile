FROM debian:7.7
MAINTAINER  Radu Graur "radu.graur@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

# install Apache2 and PHP5
RUN apt-get update \
  && apt-get install -qq wget \
  && echo "deb http://packages.dotdeb.org wheezy-php55 all" | tee -a /etc/apt/sources.list.d/php5.list \
  && wget -O- http://www.dotdeb.org/dotdeb.gpg | apt-key add - \
  && apt-get update \
  && apt-get install -qq \
    apache2 \
    libapache2-mod-php5 \
    php5 \
    php5-mysql \
    php5-mcrypt \
    php5-curl \
    mysql-client \
  && a2enmod php5 \
  && service apache2 restart \
  && apt-get purge -y --auto-remove wget \
  && rm /var/www/index.html

# add startup script
ADD ./start.sh /opt/start.sh
RUN chmod +x /opt/start.sh

# cleanup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 80
#WORKDIR /var/www
CMD ["/opt/start.sh"]
